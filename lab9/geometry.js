function calcCircleGeometries(rad) { 
    const pi = Math.PI;
    var area = pi * rad * rad; 
    var circ = 2 * pi * rad; 
    var diam = 2 * rad; 
//    var geom = [area, circ, diam];
    var geom = [area, circ, diam];
    return geom; 

    }
document.getElementById("calc").innerHTML = calcCircleGeometries(2);
document.getElementById("calc2").innerHTML = calcCircleGeometries(4);
document.getElementById("calc3").innerHTML = calcCircleGeometries(3.3);
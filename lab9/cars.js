function carArrays1() {
    var Toyota = ['Toyota', 'Highlander', 2006, '$25,000'];
    var Honda1 = ['Honda', 'Civic', 2009, '$40,000'];
    var Honda2 = ['Honda', 'Civic', 2001, '$20,000'];
    var cars = [Toyota, Honda1, Honda2];
//        cars[1];
//        cars[2];
//        cars[0];
        return cars[1];
//        return cars[2];
//        return cars[0];
}
//document.getElementById("car").innerHTML = carArrays1();

function carArrays2() {
    var Toyota = ['Toyota', 'Highlander', 2006, '$25,000'];
    var Honda1 = ['Honda', 'Civic', 2009, '$40,000'];
    var Honda2 = ['Honda', 'Civic', 2001, '$20,000'];
    var cars = [Toyota, Honda1, Honda2];
//        cars[1];
//        cars[2];
//        cars[0];
//        return cars[1];
        return cars[2];
//        return cars[0];
}
//document.getElementById("car2").innerHTML = carArrays2();

function carArrays3() {
    var Toyota = ['Toyota', 'Highlander', 2006, '$25,000'];
    var Honda1 = ['Honda', 'Civic', 2009, '$40,000'];
    var Honda2 = ['Honda', 'Civic', 2001, '$20,000'];
    var cars = [Toyota, Honda1, Honda2];
//        cars[1];
//        cars[2];
//        cars[0];
//        return cars[1];
//        return cars[2];
        return cars[0];
}
document.getElementById("car").innerHTML = carArrays1();
document.getElementById("car2").innerHTML = carArrays2();
document.getElementById("car3").innerHTML = carArrays3();